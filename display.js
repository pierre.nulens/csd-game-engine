const add_spaces = word => {
    return word
    .split("")
    .join(" ")
    .toUpperCase();
  };

  const create_display_word = (word, guesses) => {
    let display_word = "";

    for (let letter of word)
    {
      if (guesses.indexOf(letter) >= 0) display_word += letter;
      else display_word += "_";
    }

    return display_word;
  };

  let guesses = "u";

  let word = "fujitsu";
  console.log(word);