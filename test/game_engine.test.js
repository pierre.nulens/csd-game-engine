const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    const letGameState = GameEngine.startGame();
    expect(letGameState.lives).toBe(5);
    expect(letGameState.word.length).toBeGreaterThan(3);
    expect(letGameState.display_word.length).toBeGreaterThanOrEqual(2 * letGameState.word.length - 1);
  })
  test("update the guesses and lives when a wrong guess is given", () => {
    let game_state = GameEngine.startGame();
    game_state.word = "fujitsu";
    
    var letter = "a";
    
    let new_game_state = GameEngine.takeGuess(game_state, letter);
    expect(new_game_state.lives).toBe(4);
    expect(new_game_state.guesses).toContain(letter);
  });

  test("stop the game when there are 0 lives", () => {
    let game_state = GameEngine.startGame();
    game_state.lives = 1;
    game_state.word = "fujitsu";
    
    var letter = "a";

    let new_game_state = GameEngine.takeGuess(game_state, letter);
    expect(new_game_state.lives).toBe(0);
    expect(new_game_state.status).toBe("STOPPED");
  });

  // test("update the guesses and lives when a right guess is given", () => {
  //   let game_state_right = GameEngine.startGame();
  //   game_state_right.word = "fujitsu";
  //   game_state_right.display_word = "_ _ _ _ _ _ _";

  //   var letter_right = "u";
    
  //   let new_game_state = GameEngine.takeGuess(game_state_right, letter_right);
  //   expect(new_game_state.lives).toBe(5);
  //   expect(new_game_state.guesses).toContain(letter_right);
  //   expect(new_game_state.display_word).toBe("_ u _ _ _ _ u");
  // });
});
